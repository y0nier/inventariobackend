package com.nexos.inventario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nexos.inventario.model.Cargo;

public interface CargoRepository extends JpaRepository<Cargo, Long> {

}
