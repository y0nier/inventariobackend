package com.nexos.inventario.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mercancia")
public class Mercancia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String Mercancia_editada;
	private Date Fecha_edicion;
	private String NombreProducto;
	private int cantidad;
	private Date fechaIngreso;
	
	
	@ManyToOne
	@JoinColumn(name = "id_cargo")
	private Cargo cargo;
	

	public Mercancia(String mercancia_editada, Date fecha_edicion, String nombreProducto, int cantidad,
			Date fechaIngreso, Cargo usuario) {
		super();
		Mercancia_editada = mercancia_editada;
		Fecha_edicion = fecha_edicion;
		NombreProducto = nombreProducto;
		this.cantidad = cantidad;
		this.fechaIngreso = fechaIngreso;
		this.cargo = usuario;
	}


	public Mercancia() {
		
		
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getNombreProducto() {
		return NombreProducto;
	}


	public void setNombreProducto(String nombreProducto) {
		NombreProducto = nombreProducto;
	}


	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getMercancia_editada() {
		return Mercancia_editada;
	}

	public void setMercancia_editada(String mercancia_editada) {
		Mercancia_editada = mercancia_editada;
	}

	public Date getFecha_edicion() {
		return Fecha_edicion;
	}

	public void setFecha_edicion(Date fecha_edicion) {
		Fecha_edicion = fecha_edicion;
	}

	public Cargo getUsuario() {
		return cargo;
	}

	public void setUsuario(Cargo usuario) {
		this.cargo = usuario;
	}
	
	
	

}
