package com.nexos.inventario.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String nombre;
	private int edad;
	private Date fecha_contrato;
	
	@ManyToOne
	@JoinColumn(name = "id_mercancia")
	private Mercancia mercancia;
	@ManyToOne
	@JoinColumn(name = "id_cargo")
	private Cargo cargo;
	
	
	
	public Usuario(String nombre, int edad, Date fecha_contrato, Mercancia mercancia, Cargo cargo) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.fecha_contrato = fecha_contrato;
		this.mercancia = mercancia;
		this.cargo = cargo;
	}

	public Usuario() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public Date getFecha_contrato() {
		return fecha_contrato;
	}
	public void setFecha_contrato(Date fecha_contrato) {
		this.fecha_contrato = fecha_contrato;
	}
	public Mercancia getMercancia() {
		return mercancia;
	}
	public void setMercancia(Mercancia mercancia) {
		this.mercancia = mercancia;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	

}
