package com.nexos.inventario.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.inventario.model.Cargo;
import com.nexos.inventario.service.CargoService;

@RestController
@RequestMapping("/cargo/")
public class CargoREST {
	
	@Autowired
	private CargoService cargoService;
	
	@GetMapping
	private ResponseEntity<List<Cargo>> getAllcargos (){
		return ResponseEntity.ok(cargoService.findAll());
		
	}
}
